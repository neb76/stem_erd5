#8/28/2013
#a one day, debugish script, used to dtermine how to fix the plotting scripts
	# (there is some inconsistency, and specifics were lost in travels)


	work.station<-"atlas"
	if(work.station=="beast"){
		stem.dir<-"/home/neb76/nth.am.TRES/"
		source.dir<-"/home/neb76/source.sandbox/"
		stm.ave.dir <- paste(stem.dir,"stm.3km.ave/",sep="")
		plot.dir <- "/home/neb76/nth.am.TRES/ttt.fix_plots/"
	}else{
		# source("~/stem_erd5/source.wfcontrol/erd_5_params.R")
		stem.dir<-"/home/fs01/neb76/compute_1610252/Can_warb_full_Canada_Warbler/"
		# stem.dir<-"/home/fs01/neb76/modis_wink_NthAm_Tree_Swallow/"
		source.dir<-"/home/fs01/neb76/stem_erd5/source.wf/"
		stm.ave.dir <- paste(stem.dir,"stm.3km.ave/",sep="")
		plot.dir <- "/home/fs01/neb76/ttt.debug_legend_placement/"
		.libPaths("/home/fs01/neb76/R_libs.new/")
		library(fields)
		library(maps)
		# library(multicore)
		library(mgcv)
	}

	source(paste(source.dir,"st.matrix.map.library.ttt.R",sep=""))
# source(paste(source.dir,"wf.control_experiment.R",sep=""))
# source("~/stem_erd5/fix.sandbox/decipher_plots_extended.inits.R")
	load(paste(stm.ave.dir,"srd.3km.stm.ave.RData",sep=""))
	load(paste(stem.dir,"pp.ave/train.pred.fold.ave.RData",sep=""))
	pred.fold.ave<-train.pred.fold.ave

	# load(paste(stem.dir,"pp.ave/",sep=""))
	str(srd.stm.ave)#sanity, see if loaded
	#-------------------------------------------------------------------------------
	# params
	#-------------------------------------------------------------------------------
	# map.plot.pixel.width <- 2500
	# ns.3km.rows<-555 #pos-iss


	quebec.extent<- list(type = "rectangle",
		lat.min = 44.78,  
		lat.max = 57.42,
		lon.min = -83.23,
		lon.max = -59.41)

	north.america.extent <- list(type = "rectangle",
		lat.min = 2,  
		lat.max = 72,
		lon.min = -170,
		lon.max = -42)


	can.warb.extent <- list(type = "rectangle",
		lat.min =-9,
		lat.max =63,
		lon.min =-142,
		lon.max =-50)

	spatial.extent.list<-can.warb.extent



	#-------------------------------------------------------------------------------
	# attempt to put in enough directories to get buy
	#-------------------------------------------------------------------------------

diag.dir<-"/home/fs01/neb76/compute_1610252/Can_warb_full_Canada_Warbler/threshold/diagnostics/"
species.name<-"Canada_Warbler"
stm.ave.plot.dir<-plot.dir
stm.ave.plot.diag.dir<-plot.dir
map.plot.pixel.width <- 2500	
plot.type<-"PNG"

date.seq <- srd.stm.ave$times
jdate.seq <- round((date.seq - floor(date.seq))*365)
year.seq <- floor(date.seq)		
# Convert Temporal Sequence times to POSIX
p.time <- strptime( x=paste(round(jdate.seq),year.seq), "%j %Y")
month.text <- months(p.time, abbreviate = FALSE)
date.names <- paste(month.text, " ",p.time$mday,sep="") 

	