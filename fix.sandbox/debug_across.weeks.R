

debug_across<-function(ttttt){
		ir <- rep(NA,52)
		ir.sampled <- rep(NA,52)
		pnr <- rep(NA,52)
		pnr.sampled <- rep(NA,52)
		pss <- rep(NA,52)
		pss.sampled <- rep(NA,52)
		# Read and format IR Data 
		diag.dir.list <- list.files(path=diag.dir)

		for (iii.spp in  c(1:52)) { 
			# Read Predictive Perf. vs Threshold Statistics	
			filename.ttt <- paste(species.name, 
								".monthly.pp.",iii.spp,".csv",sep="")
			if ( sum(diag.dir.list  == filename.ttt) == 0 ) next
			monthly.map.pp <- read.csv(file = paste(diag.dir, filename.ttt, sep=""))
			# Extract IR stats
			ir[iii.spp] <- mean(monthly.map.pp$ir, na.rm=T)
			ir.sampled[iii.spp] <- mean(monthly.map.pp$ir.sampled, na.rm=T)
			pnr[iii.spp] <- mean(monthly.map.pp$pnr, na.rm=T)
			pnr.sampled[iii.spp] <- mean(monthly.map.pp$pnr.sampled, na.rm=T)
			pss[iii.spp] <- mean(monthly.map.pp$pss, na.rm=T)
			pss.sampled[iii.spp] <- mean(monthly.map.pp$pss.sampled, na.rm=T)
		}	
		# Plot
		map.file.name <- paste(diag.dir, species.name, ".ir.ss.png" ,sep="")  
		plot.inch.width <- 6
		plot.inch.height <- 3
		if (plot.type == "pdf") {
			pdf(file = map.file.name, 
				width = 6, 
				height= 3)
		}
		if (plot.type == "PNG") {
			png(file = map.file.name, 
				width = 1000, 
				height= 500,
				units = "px")
		}
		if (plot.type == "bitmap") {
			bitmap(file = map.file.name, 
				width = 1000, 
				height= 500,
				units = "px",
				res = 300)
		} 
		# ---------------------
		par(mfrow = c(1,2))
		plot( pnr.sampled,
			ylim = c(0,0.30), 
			type ="b", 
			col="red",
			main ="Sample Size Ratio Positive:Negative")
		lines(pnr) 
		plot( ir/100,
			#ylim = c(0,0.30), 
			type ="b", 
			col="black",
			main ="Influence Ratio Positives:Negatives")
		lines(ir.sampled, col="red") 
		dev.off()

	alpha <- (target.influence/ir.sampled)
	# Make adjustment ONLY for weeks where Influene ratio <= target.influence
	alpha[ ir.sampled > target.influence ] <- 1 
	# --------------------------
	weekly.thresh <- rep(NA,52)
	weekly.adj.thresh <- rep(NA,52)
	diag.dir.list <- list.files(path=diag.dir)
	for (iii.spp in  c(1:52)) { #c(1:length(jdate.seq))){	
		# Read Predictive Perf. vs Threshold Statistics	
			filename.ttt <- paste(species.name, 
								".monthly.pp.",iii.spp,".csv",sep="")
			if ( sum(diag.dir.list  == filename.ttt) == 0 ) next
			monthly.map.pp <- read.csv(file = paste(diag.dir, filename.ttt, sep=""))
			unique.thresh <- sort(unique(monthly.map.pp$threshold))
			##finding unique positive sample sizes
			unique.pss<-sort(unique(monthly.map.pp$pss))
			print("unique values for the positive sample size")
			print(unique.pss)
		# Find Threshold that Maximizes mean of sensitivity and specificity
			# Note - This is not really the TSS, but is the 
			# average of sensitivity and specificity
			tss.ttt <- (monthly.map.pp$sensitivity + 
							monthly.map.pp$specificity )/(2)
			ttt.mean <- tapply(tss.ttt, monthly.map.pp$interval, mean, na.rm=T)
			ttt <- as.numeric(which.max(ttt.mean))
			max.seq.thresh <- unique.thresh[ttt]
			weekly.thresh[iii.spp] <- max.seq.thresh 
		# ----------------------------------------------------------------------	
		# Compute IR weighted average of sensitivity and specificity
		# alpha describes relative weight on sensitivity
		# ----------------------------------------------------------------------
			tss.adjusted.ttt <- (monthly.map.pp$sensitivity * alpha[iii.spp]   + 
								monthly.map.pp$specificity )/(alpha[iii.spp] + 1)
			ttt.adj.mean <- tapply(tss.adjusted.ttt , monthly.map.pp$interval, mean, na.rm=T)
			ttt <- as.numeric(which.max(ttt.adj.mean))	
			cat("rank of threshold for week: ",iii.spp,"\n")
			print("(if 2, means the first non-zero picked")
			print(ttt)
			max.seq.thresh.adj <- unique.thresh[ttt]
			weekly.adj.thresh[iii.spp] <- max.seq.thresh.adj 
		# -------------------------------------
		# Plot TSS vs Threshold
		# -------------------------------------	
		map.file.name <- paste(diag.dir, species.name, ".tss.adj.v.threshold.",iii.spp,".png" ,sep="") 
		if (plot.type == "PNG") {
				png(file = map.file.name, 
					width = 1000, #map.plot.pixel.width, 
					height= 1000, #map.inits$map.plot.height,
					units = "px")
					plot.cex.c <- 1.0
					count.map.toggle <- T
		}
		if (plot.type == "bitmap") {
				bitmap(file = map.file.name, 
					width = 1000, #map.plot.pixel.width, 
					height= 1000, #map.inits$map.plot.height,
					units = "px",
					res = 300)
					plot.cex.c <- 0.5
					count.map.toggle <- T
		} 
		par( cex=1.0)

		plot(	monthly.map.pp$threshold,
				tss.adjusted.ttt, 
				xlim = c(0, unique.thresh[length(unique.thresh)-1]),
				cex=0.5, pch=19, col="lightblue",
				xlab = "Threshold",
				ylab = "TSS",
				main = paste("TSS=", format(max.seq.thresh, digits=5),
							"  Adjusted TSS= ", format(max.seq.thresh.adj, digits=5)))
		lines(rep(max.seq.thresh.adj,2), c(-1,1), lwd=1, col="blue")	
		# ---
		points(jitter(monthly.map.pp$threshold), tss.ttt, 
				cex=0.5, pch=19, col="orange")
		lines(rep(max.seq.thresh,2), c(-1,1), lwd=1, col="red")
		# ------------------
		dev.off()
	} # iii.spp
	
}


