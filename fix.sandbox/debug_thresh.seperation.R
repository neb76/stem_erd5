th<-read.csv('/home/fs01/neb76/ttt.debug_thresh_2/Can_warb_full_Canada_Warbler/threshold/diagnostics/Canada_Warbler.thresholds.weekly.csv')
difs<-th[,3]-th[,2]



#preds that are non zero
#
library(ggplot2)
thresh.values <- read.csv('/home/fs01/neb76/ttt.debug_thresh_2/Can_warb_full_Canada_Warbler/threshold/diagnostics/Canada_Warbler.thresholds.weekly.csv')



plot_cuts <-function(week_id){
	iii.spp<-week_id
	cat(" Week = ", iii.spp, " beginning processing .... \n")	
# --------------------------------------------------------
# Select Time Window  
# --------------------------------------------------------
	begin.mdate <- jdate.seq[iii.spp] - test.interval
	end.mdate <- jdate.seq[iii.spp] + test.interval
	# SRD Limit To Times in Window
	if ( begin.mdate > 0 & end.mdate < 366) 
		ttt.index <- jdate.seq >= begin.mdate & jdate.seq <= end.mdate
	if ( begin.mdate < 0 & end.mdate < 366) 
		ttt.index <- jdate.seq >= (begin.mdate+366) | jdate.seq <= end.mdate
	if ( begin.mdate > 0 & end.mdate > 366) 
		ttt.index <- jdate.seq >= begin.mdate | jdate.seq <= (end.mdate - 366)
	srd.time.seq <- c(1:length(srd.stm.ave$times))[ttt.index]
	# Predictions Limit To Times in Window
	if ( begin.mdate > 0 & end.mdate < 366) 
		date.index <- pred.fold.ave$date >= begin.mdate & 
					pred.fold.ave$date <= end.mdate
	if ( begin.mdate < 0 & end.mdate < 366) 
		date.index <- pred.fold.ave$date >= (begin.mdate+366) | 
					pred.fold.ave$date <= end.mdate
	if ( begin.mdate > 0 & end.mdate > 366) 
		date.index <- pred.fold.ave$date >= begin.mdate | 
					pred.fold.ave$date<= (end.mdate - 366)
# --------------------------------------------------------
# Check to make sure that there is sufficient data for Selecting Thresholds
# --------------------------------------------------------	

	if (sum(date.index,na.rm=T) < pp.min.testset.sample.size) {
		cat( "ERROR: Interval ", iii.spp, 
			": Insufficient sample size for selecting thresholds.","\n")
		next
	}
	pa.data <- data.frame(tag="global", 
					obs = as.numeric(pred.fold.ave$obs[date.index]), 
					ppp = pred.fold.ave$mean[date.index])
	if ( var(pa.data$obs,na.rm=T)  <= 0) {
		cat( "ERROR: Interval ", iii.spp, 
			": Only a single class present for selecting thresholds. ","\n")
		next
	}	

	max.seq.thresh_2 <- thresh.values$threshold.ir.adj[week_id] 
	print(max.seq.thresh_2)

	ttt.non_null<-pa.data$ppp>0
	non_null.data<-na.omit(pa.data[ttt.non_null,])
	non_null.data[order(-non_null.data$ppp),]
	# plot.name<-paste("jitter_see.cutt_week.",week_id,".png",sep="")
	plot.dir<-"/home/fs01/neb76/ttt.debug_thresh_2/home_plots/with.jit_thresh.line_2/"
	plot.name_2<-paste(plot.dir,"see_thresh.3.",week_id,".png",sep="")
	chart<-ggplot(non_null.data,aes(x=ppp,y=factor(obs),fill=factor(obs))) + geom_jitter(aes(colour=factor(obs))) +
		geom_vline(xintercept=max.seq.thresh_2)
	png(plot.name_2)
	print(chart)
	dev.off()
	# ggsave(filename=plot.name_2,plot=chart)# dev.off()

} # end iii.season


mclapply(as.list(1:52),plot_cuts)


