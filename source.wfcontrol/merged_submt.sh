#!/bin/bash

#careful, syntax changed in update from 
	#rootnum-arrayid
		# to:
	#rootnum[arrayid]

#changes to make if running multiple jobs at once:
#	1) eperiment specific names to replace /intermediate/stem
#	2) comment out last line in wf.atas_exp2.pbs:
#			mv $HOME/stem/source.wfcontrol/print.* $HOME/$EXP_DIR_NAME/logs   

#other improvement:
	# 1)handling of failed folds
	# 	-dependecy tags, (#PBS -W depend=afterok:jobID)
	# 	this means the job on which its set needs lead job to finish wihout error
	# 	depend=afterany:jobID  will allow jobs to begin on any exit, error or not

	# 	logic in summary step does handle for missing files
	# 		-so either, write a resubmission script,
	# 		-or just do the summary on the succesful folds

#notes:
	#  the screen prints are stored as print.<phase_name> 
	#  all are moved to the ultimate job result folder, except for the summary step, 
	#  which is left in the ~/stem/source.wfcontrol directory


#######
#### important! if array size doesn't match fold number, this will fail

echo 'cleaning up past jobs...'
#exp_name written in ..prep2.execution.R
#
rm -r ~/{exp_name,exp_uniq_postfix}.txt

#'instantiate jobs', put them on queue with holds, storying their job names
# they will be on queu, but not in a running state
echo 'creating new jobs...'
ARRAY_SIZE=200
FIRST=$(qsub -h $HOME/stem_erd5/source.wfcontrol/wf.atlas_data.prep2.pbs)
#below constant used to make each jobs execution independent on network file system
FIRST_NUM=${FIRST//[^0-9]/} 

SECOND=$(qsub -h -v INTER_DIR_POSTFIX=$FIRST_NUM -t 1-$ARRAY_SIZE $HOME/stem_erd5/source.wfcontrol/wf.atlas_exp1.pbs)
THIRD=$(qsub -h -v INTER_DIR_POSTFIX=$FIRST_NUM $HOME/stem_erd5/source.wfcontrol/wf.atlas_exp2.pbs)
#get the 'root' job_id, as arrays append like: job_id-1, job_id-2...job_id-n
SECOND=${SECOND//[^0-9]/} #strip non digits
sleep 10  #pause for 10 seconds
echo "data_prep2  ----->    $FIRST" 
echo "folds:      ----->    "$SECOND"-FOLD#"
echo "summary     ----->    $THIRD" 

#use qalter to set dependency PBS option.  These command line arguement are 
# same as the lead #PBS commands in submission scripts
echo "setting depedencies..."
####job 2
for ((x=1; x<=$ARRAY_SIZE; x++))
do 
	`qalter -W depend=afterok:$FIRST $SECOND[$x]`
done

####job 3
ARRAY_CHAIN=$(
for ((x=1; x<$ARRAY_SIZE; x++))
do 
	echo $SECOND"["$x"]:"
done
echo $SECOND"["$ARRAY_SIZE"]"
)
ARRAY_CHAIN=$(echo $ARRAY_CHAIN | tr -d ' ') #remove white space
qalter -W depend=afterok:$ARRAY_CHAIN $THIRD
sleep 5 #pause for 5 seconds
echo $ARRAY_CHAIN

##with dependencies in place, take off user hold
#jobs with dependencies will continue to wait, and appear same on the queue
###
echo "starting jobs..."
qrls $FIRST 
for ((x=1; x<=$ARRAY_SIZE; x++))
do 
	`qrls $SECOND[$x]`
done
sleep 5 #pasue for 5 seconds
qrls $THIRD

echo "all set"
exit 0
