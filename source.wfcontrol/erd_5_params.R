

# --------------------------------------------------------------------------
# Workflow Configuration and Initialization
# --------------------------------------------------------------------------
	# parent.dir = All eBird STEM resources and workflow products live here. 
	# source.dir = location of R files - stem code
	# source.control.dir = location of control files for experiments (R & CSV files)
	# ---------------------	
	#parent.dir <- "/Users/Fink/projects/2011_TG_Atlas/Calibration_Test/"
	#parent.dir <- "/mnt/data3/TG_2011/Calibration_Test/2/"
	# COLUMBA
	#parent.dir <- "~/TG/"
	# LONESTAR
	#parent.dir <- paste(Sys.getenv("WORK"), "/stem/",sep="")
	# ATLAS
		# Get Directories from Environment Variables
 	# tmp.dir <- Sys.getenv("TMPDIR")
 	#cat(" This is the TMPDIR from qsub \n")
 	#cat("tmp.dir=",tmp.dir,"\n")
 	# parent.dir <- paste("~/stem/",sep="")		
 	# erd5.parent.dir<-"~/erd_5/"

 	tmp.dir<-Sys.getenv("TMPDIR")
 	
 	erd5.parent.dir<-paste(tmp.dir,"/erd_5/",sep="")
 	MAKING_BIG_MEM<-F
 	if(MAKING_BIG_MEM){
		source.dir <- paste(erd5.parent.dir, "source/", sep="")
		source.control.dir<-source.dir
 	}else{
 		parent.dir<-paste(tmp.dir, "/stem/",sep="") #looks like bug, as stem not stem_erd5
 			#however! not the case, as the copying puts all in an internal temp/stem/, not temp/stem_erd5 
 		source.dir<-paste(parent.dir, "/source.wf/",sep="") 
 		source.control.dir<-paste(parent.dir,"/source.wfcontrol/",sep="")
 	}
 	
	# Add in local library path
	.libPaths("/home/fs01/neb76/R_libs.new/")
	# ---------------------
	# ---------------------
	# Data Directories
	# ---------------------	
	bigmem.data.dir <- paste("~/data.erd.srd.5.0/bigmem/", sep="")
	# exp.rdata.dir 	<- "~/ttt.data_production/"
	exp.rdata.dir 	<- paste(parent.dir, "data.experiment/", sep="")
		dir.create(path = bigmem.data.dir, showWarnings =FALSE)	
		dir.create(path = exp.rdata.dir, showWarnings =FALSE)
	# ---------------------
	# ERD & SRD Bigmem filename tags
	# ---------------------	
	erd.data.tag <- "erd.5.0"  	
	srd.3km.tag <-  "srd_point_data_3km_v5.0"
	# ----------------------
	# Experiment Job Control 
	# 	 We need to read information about experiment 
	#	 because RData files are organized by experiment. 
	# ----------------------
		experiment.control.filename <- paste(source.control.dir, 
						 "exp.bz.ovenbird.07.15.13.csv",sep="")
						#"exp.bz.02.07.12.csv",sep="")
						#"exp.Eastern.parameters.02.03.12.csv", sep="")
						#"exp.scale.search.01.25.12.csv", sep="")
		# Bootsrap sampling design - bootstrap design is shared across analyses
		exp.samples.tag <- "bz.exp"
		# Set "job.id" == 1 for initialization
		# job.id indexes the experiments (columns) in csv job control file. 
		job.id <- 1  
		# This file processes & reads inits for experiments
		print("before: wf.control_experiment.R")   ###debug, remove
		source(paste(source.dir,"wf.control_experiment.R",sep=""))
		print("after: wf.control_experiment.R")    ###debug, remove
		srd.resolution.code <- as.character(job.list$SRD_RESOLUTION[job.id])
	# ----------------------
	# Plotting 
	# ----------------------		
	# For platform differences we have "PNG" or "bitmap" 
	plot.type <- "PNG" 
	# Size of maps
	map.plot.pixel.width <- 2500

# -------------------------------------------------------
# THESE ARE PARAMETERS THAT CHANGE WITH ST EXTENT
# -------------------------------------------------------
	# --------------------------------------------
	# STM Spatial Prediction Design & Plotting
	# --------------------------------------------		
		# It will depend on the value of "job.id" & "srd.resolution.code"
		#125 for continental US == 25 degrees lat & 62 degrees lon
		# ns.30km.rows <-   125
		#555 for continental US == 25 degrees lat & 62 degrees lon
		# ns.3km.rows <-   555
		ns.3km.rows <-   900  #for the full Northern hemisphere extent
		#this hard coding makes me nervous...
	# --------------------------------------------	
	# Predictive Performance - ST Design for   
	# -------------------------------------------- 
		# Compute Temporal Window Predictive Performance 
		# with Uniform Eq. Wt. Subsampling 
		# -------------------------------------------- 
		# Number of time intervals for PP calcs - eg. weekly or monthly
		pp.n.time.intervals <- 12
		# Number of MC trials for test point selection
		pp.n.mc <- 25
		pp.min.testset.sample.size <- 25
		# Sampling grid size
		pp.grid.cell.min.lat <- 1
		pp.grid.cell.min.lon <- 1 
		pp.max.cell.locs <- 2 
		# National Settings
		#pp.grid.cell.min.lat <- 0.5
		#pp.grid.cell.min.lon <- 0.5 
		#pp.max.cell.locs <- 2
	# --------------------------------------------	
	# Predictor Impotance ST Interpolation   
	# -------------------------------------------- 
		# wf.exp3_make.pi.stm.R
		# ----------------------------
		# The resolution of the PI ST interpolation is determined
		# by the spatiotemporal resolution of the STIXEL centroids.
		# ----------------------------- 
		# Spatial Resolution
		# -----------------------------	
		# These two parameters define the spatial prediction grid. 
		# Same units as location data.frame, here, degrees	
		#lon.pred.cell.width <- 5  
		#lat.pred.cell.width <- 5 
		# These parameters need to change with the spatial extent
		# of the anaylsis. Smaller for smaller extents and larger
		# for larger extents.  For the US-continent wide TG ATLAS project
		# I use half-degree "pixels" 
			lon.pred.cell.width <- 1.0  # Same units as location data.frame
			lat.pred.cell.width <- 1.0  # Here units are degrees
		# ----------------------------- 
		# Temporal Resolution 
		# -----------------------------		
		# The temporal resolution of the smooth is controled
		# by the smoothness of the GAM interpolation. 
		# We control the smoothness of GAM
		# by setting the minimum number of data points 
		# to be smoothed and the number of knots 
		# used in the smooth. 
		# 
		# Example: These are minimum values!
		#	pi.stm.min.data.size <- 25
		#	pi.stm.temporal.knots <- 10
		# Examples: National STOB ==> 
			pi.stm.min.data.size <- 40 
			pi.stm.temporal.knots <- 20	

# --------------------------------------------
# Data Prep - Stage 1 Parameters
# --------------------------------------------	
# Since the first stage of the Data Prep 
# will be run less often, I have segregated parameters
# specific for this part of the workflow in this section. 
# -------------------------------------------- 
	# ---------------------
	# CSV Data Directories
	# ---------------------	

	csv.erd.data.dir <- paste("~/data.erd.srd.5.0/doc/", sep="")
	csv.srd.data.dir <- paste(erd5.parent.dir, "raw_srd/", sep="")
	# ---------------------
	# SRD CSV Inputs
	# ---------------------	 
	# srd.30km.csv.filename <- paste(csv.srd.data.dir, 
	# 	"srd_point_data_30km_v3.0.csv",sep="")

	srd.30km.csv.filename<-NA	
	srd.3km.csv.filename <- paste(csv.srd.data.dir, 
		"srd_point_data_1.5km.csv",sep="")
	# ------------------------------------------------------
	# eBird Data Processing Parameters
	# 	* Define Response(s)
	# 	* Define Predictors
	# 	* Define Spatial Extent ???
	# 	* Define Temporal Extent ???
	# 	* Protocol
	# 	* effort filters
	# ------------------------------------------------------
	protocol.tag <-  c("traveling","stationary") #, "casual")  
	max.effort.hrs <- 3.0
	max.effort.distance.km <- 8.1
	min.obs.time <- 5  #5AM
	max.obs.time <- 20 #8PM
	response.type <- c("occurrence")   # "abundance"
	year.list <- c(2004:2012)  # ERD3: c(2004:2010)  # ERD4: c(2004:2011) #ERD5
	n.top.spp <- 1000  # number top species	
	response.family <- "bernoulli"
	# if the response.family is not == "bernoulli" then the counts 

	# ----------------------------------------------------------------------
	# Train:Test Set Split :   
	# ----------------------------------------------------------------------
	# The function used to select unique locations 
	# based on achieving a maximum spatial coverage for the Training Set
	# We want the maximum coverage in the TRAINING set to achieve
	# the best possible estimates across the spatial extent. 
	#
	# Note that the max coverage function selects max coverage 
	# for the test set, so we use a small % as training data and 
	# wf.data.prep1_erd.traintest.split.R interchanges train & test indices. 
	# Becasue of the volume of data, we can keep a small percentage 
	# for validation and possible future parameter selection and performance 
	# evaluation.
	# ----------------------------------------------------------------------
	split.par.list <- list(grid.cell.min.lat = 0.75,
                      grid.cell.min.lon = 0.75,
                      min.val.cell.locs = 2,
                      fraction.training.data = 0.10,
                      mfrac=1.0,
                      plot.it=FALSE)

	# --------------------------------------------------------------------------
	# Predictor Name Tags
	# --------------------------------------------------------------------------
#direct copy of covariates from checklist
checklist.covariate.names<-
	c("SAMPLING_EVENT_ID", 
	"LATITUDE",
	"LONGITUDE",
	"YEAR", 
	"MONTH",
	"DAY",
	"TIME", 
	# "COUNTRY",
	# "STATE_PROVINCE",
	# "COUNT_TYPE",     #is this correct???  looks lik yes, after consideration
	"EFFORT_HRS",
	"EFFORT_DISTANCE_KM",
	"EFFORT_AREA_HA", 
	"OBSERVER_ID",
	"NUMBER_OBSERVERS",
	"GROUP_ID", 
	"PRIMARY_CHECKLIST_FLAG")

	core.covariate.names <- NA

	#intitializes building up of list, bottom two.
	extended.covariate.names <- c("ASTER2011_DEM",
		"UMD2011_LANDCOVER")

	# # ----------------------------------------------------------------------
	# Class Level Statistics
	# ----------------------------------------------------------------------
	# NLCD.class.neighborhood.size <- c("1500") # 75, 7500
	MODIS.class.neighborhood.size <- c("1500") # 75, 7500


	# NLCD.names<-c(
	MODIS.names<-c(
		"UMD2011_FS_C0",
		"UMD2011_FS_C1",
		"UMD2011_FS_C2",
		"UMD2011_FS_C3",
		"UMD2011_FS_C4",
		"UMD2011_FS_C5",
		"UMD2011_FS_C6",
		"UMD2011_FS_C7",
		"UMD2011_FS_C8",
		"UMD2011_FS_C9",
		"UMD2011_FS_C10",
		"UMD2011_FS_C12",
		"UMD2011_FS_C13",
		"UMD2011_FS_C16"

		)
	# This is an incomplete list
	class.level.fragstats <- c("PLAND", "ED", "LPI", "PD")
	# ----------------------------------------------------------------------
	# Landscape Level FragStat Tags
	# ----------------------------------------------------------------------
	landscape.fragstat.tags <- c("PLAND", "ED", "LPI", "PD")
	landscape.neighborhood.size <- c("1500")
	# ----------------------------------------------------------------------
	# Specify factors (in core.covariates) and define levels
	# ----------------------------------------------------------------------
	factor.levels <- list()	
