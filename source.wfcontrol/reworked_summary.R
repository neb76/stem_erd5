 
# -------------------------------------------------------------------------------
# -------------------------------------------------------------------------------
# eBird HPC Fold Summary Analses
# Nick Bruns extending Daniel Fink 
# CLO 

####################################
######execution_script block########
####################################
# --------------------------------------------------------------------
# 0 Load Data Prep Inits	
# --------------------------------------------------------------------
tmp.dir <- Sys.getenv("TMPDIR")
parent.dir <- paste(tmp.dir,"/stem/",sep="")		
source(paste(parent.dir,				  
"source.wfcontrol/",
"wf.atlas_bz.ovenbird_parameters.R", sep=""))
# --------------------------------------------------------------------
# 1 Fold Analysis
# --------------------------------------------------------------------
# --------------------------------------------
# --------------------------------------------
job.id <- 1  # point to first species in csv control file. 
source(paste(source.dir,"wf.control_experiment.R",sep=""))
# --------------------------------------------------------------------
# 2 Fold Summary 	
# --------------------------------------------------------------------	
fold.list <- c(1:200) 
# source(paste(source.dir, "wf.exp2_fold.summary.R",sep=""))
####################################
######/execution_script block########
####################################




	source(paste(source.dir,"vMR.library.ttt.01.07.12.R",sep=""))
	source(paste(source.dir,"vMR_baseModels.12.05.R",sep=""))
	source(paste(source.dir,"stem.gbm.predictor.importance.R",sep=""))
	source(paste(source.dir,"stem.gbm.partial.dependence.R",sep=""))
	source(paste(source.dir,"stem.gbm.fit.R",sep=""))
	source(paste(source.dir,"event.logger.R",sep=""))
	source(paste(source.dir,"eq.wt.pp.R",sep=""))
	source(paste(source.dir,"st.matrix.map.library.R", sep=""))
	#source(paste(source.dir,"bv.support.functions.R",sep=""))	
	require(PresenceAbsence)	
	require(fields)
	require(gbm)
	require(mgcv)
	require(maps)
	require(multicore)

	model.dir <- paste(stem.dir,"/model/",sep="")
	stm.30km.dir <- paste(stem.dir, "/stm.30km/",sep="")
	stm.3km.dir <- paste(stem.dir, "/stm.3km/",sep="")
	pp.dir <- paste(stem.dir, "/pp/",sep="")
	pi.dir <- paste(stem.dir, "/pi/",sep="")
	pd.dir <- paste(stem.dir, "/pd/",sep="")	
	
# --------------------------------------------
# Event Logging	
# --------------------------------------------
	log.file <- paste(stem.dir, "fold.summary.log.txt", sep="")
	log.con <- event.logger(file.name = log.file, initiate = T)

# ----------------------------------------------------------------------------- 
# Plot 3km SRD Distribution Estimates - 
# -----------------------------------------------------------------------------
	# ---------------------------------
	# Set Plotting Inits
	# ---------------------------------
	map.filename.tag <- "sdm.3km."
	stm.dir <- paste(stem.dir, "/stm.3km/",sep="")
	load(file=paste(exp.rdata.dir, exp.tag, ".srd.3km.locations.RData",sep=""))    
	stm.ave.dir <- paste(stem.dir, "/stm.3km.ave/",sep="")
	dir.create(stm.ave.dir, showWarnings = F)
	# ------------------------------------------------------------------------ 
	# Design Average ST Matrix of Species Distribution Estimates
	# ------------------------------------------------------------------------
	srd.stm.ave <- list(
		locs = data.frame(
		lat= locations$lat, 
		lon= locations$lon),
		times = date.seq,
		mean = matrix(0, length(locations$lat), length(jdate.seq)),
		var = matrix(0, length(locations$lat), length(jdate.seq)),
		count =  matrix(0, length(locations$lat), length(jdate.seq))   )
	# Average over data samples	
	for (jjj in c(1:n.samples)){
	 	f.begin <- (jjj-1)*n.replicates+1
	 	f.end <- jjj*n.replicates
		ttt.mean <- matrix(0, length(locations$lat), length(jdate.seq))
		ttt.count <- matrix(0, length(locations$lat), length(jdate.seq))
		# Average over partition replicates within samples 
		for (fff in c(f.begin:f.end)){
			load(file= paste(stm.dir,"srd.3km.stm.",
				fold.list[fff],".RData", sep=""))
			fff.index <- !is.na(srd.stm$mean)
			ttt.count[fff.index] <- ttt.count[fff.index] + 1
			    ttt.mean[fff.index] <- ttt.mean[fff.index] + 
			    	srd.stm$mean[fff.index]
			    rm(fff.index, srd.stm)          
		}
		na.ind <- (ttt.count > 0)
		ttt.mean[na.ind] <- ttt.mean[na.ind]/ttt.count[na.ind]
		ttt.index <- !is.na(ttt.mean)	        
		srd.stm.ave$count[ttt.index] <- srd.stm.ave$count[ttt.index] + 1 
		srd.stm.ave$mean[ttt.index] <- srd.stm.ave$mean[ttt.index] + 
						    ttt.mean[ttt.index]  
		srd.stm.ave$var[ttt.index] <- srd.stm.ave$var[ttt.index] + 
						    (ttt.mean[ttt.index]) ^ 2 
		rm(ttt.mean, ttt.count, ttt.index, f.begin, f.end)
	} #jjj 
	na.ind <- (srd.stm.ave$count > 0)
	srd.stm.ave$mean[na.ind] <- srd.stm.ave$mean[na.ind]/
						srd.stm.ave$count[na.ind]
	na.ind2 <- (srd.stm.ave$count >= 2)
	srd.stm.ave$var[na.ind2] <- srd.stm.ave$var[na.ind2]/ 
			(srd.stm.ave$count[na.ind2] -1) -
			(srd.stm.ave$mean[na.ind2]^2)*(srd.stm.ave$count[na.ind2])/
			(srd.stm.ave$count[na.ind2] -1)
	# Save srd.stm.ave
	save(srd.stm.ave, file= paste(stm.ave.dir,"srd.3km.stm.ave.RData", sep=""))
	event.logger(event.tag = "3km design average completed", con = log.con)

# //////////
	
	
	mcGet_averaged_bootstraps<-function(jjj){
	 	f.begin <- (jjj-1)*n.replicates+1
	 	f.end <- jjj*n.replicates
		ttt.mean <- matrix(0, length(locations$lat), length(jdate.seq))
		ttt.count <- matrix(0, length(locations$lat), length(jdate.seq))
		# Average over partition replicates within samples 
		for (fff in c(f.begin:f.end)){
			load(file= paste(stm.dir,"srd.3km.stm.",
				fold.list[fff],".RData", sep=""))
			fff.index <- !is.na(srd.stm$mean)
			ttt.count[fff.index] <- ttt.count[fff.index] + 1
			    ttt.mean[fff.index] <- ttt.mean[fff.index] + 
			    	srd.stm$mean[fff.index]
			    rm(fff.index, srd.stm)          
		}
		na.ind <- (ttt.count > 0)
		ttt.mean[na.ind] <- ttt.mean[na.ind]/ttt.count[na.ind]
		ttt.mean[!na.ind]<-NA
		return(ttt.mean)
		} #jjj 

	mcGet_averaged_bootstraps_2<-function(jjj){
	 	f.begin <- (jjj-1)*n.replicates+1
	 	f.end <- jjj*n.replicates
		# Average over partition replicates within samples 
		rep_copies<-list()
		mat_count<-1
		for (fff in c(f.begin:f.end)){
			load(file= paste(stm.dir,"srd.3km.stm.",
				fold.list[fff],".RData", sep=""))
			rep_copies[[mat_count]]<-srd.stm$mean
		}
		ttt_mean<-apply(simplify2array(rep_copies),c(1,2),mean,na.rm=T)
		ttt_count<-Reduce('+',lapply(rep_copies,function(x){!is.na(x)}))  #convert to logical, count these
		na.index<-ttt_count==0
		ttt_mean[na.index]<-NA
		return(ttt_mean)
	}


#get list of all, put NA if zero
#
out_list<-mclapply(as.list(1:n.samples),mcGet_averaged_bootstraps)
fit_count<-Reduce('+',lapply(out_list,function(x){!is.na(x)}))
fit_var<-apply(simplify2array(out_list),c(1,2),var,na.rm=T)
fit_mean<-apply(simplify2array(out_list),c(1,2),mean,na.rm=T)
srd.stm.ave <- list(
		locs = data.frame(
			lat= locations$lat, 
			lon= locations$lon
			),
		times = date.seq,
		mean = fit_mean,
		var = fit_var,
		count =  fit_count	)

	save(srd.stm.ave, file= paste(stm.ave.dir,"srd.3km.stm.ave.RData", sep=""))
	event.logger(event.tag = "3km design average completed", con = log.con)
#/////////////
	# ------------------------------------------------------------------------ 
	# ST Matrix Maps (AKA Halloween Maps) 
	#
	# The ns.rows parameter controls the number of rows
	# used by image.plot. The larger the number, the smaller the pixels. 
	# note, if ns.rows is too big, the you risk plotting empty pixels where
	# there are no srd locations.
	# z.max is the maximum predicted probabilty shown on the 
	# map legend. Species specific parameter adjustment needed. 
	# ------------------------------------------------------------------------
	stm.ave.dir <- paste(stem.dir,"stm.3km.ave.plot/",sep="")
	dir.create(stm.ave.dir, showWarnings = F)
	map.inits <- initizalize.map.grid(
		spatial.extent = spatial.extent.list,
		map.plot.width = map.plot.pixel.width, 
		pred.grid.size = map.plot.pixel.width)	
	# for (iii in 1:length(srd.stm.ave$times)){
	mc_plot_wrapper<-function(iii){
		cat("plotting prediction number: ", iii, "\n")
		num.txt <- formatC( jdate.seq[iii] , format="fg", width= 3)
		num.txt <- chartr(" ","0",num.txt)
		map.file.name <- paste(stm.ave.dir, map.filename.tag,
						year.seq[iii],".",num.txt,".png" ,sep="") 
		if (plot.type == "PNG") {
			png(file = map.file.name, 
				width = map.plot.pixel.width, 
				height= map.inits$map.plot.height,
				units = "px")
				#res = 300) 
		}
		if (plot.type == "bitmap") {
			bitmap(file = map.file.name, 
				width = map.plot.pixel.width, 
				height= map.inits$map.plot.height,
				units = "px",
				res = 300) 
		} 
		par(oma=c(2,2,2,2), cex=1.0)
		ns.rows <- ns.3km.rows
		st.matrix.map(	x = srd.stm.ave$locs$lon, 
					y = srd.stm.ave$locs$lat,
					z = srd.stm.ave$mean[,iii],
					image.gridcell.height = ns.rows,
					zlim = c(0, quantile(srd.stm.ave$mean, prob=0.95, na.rm=T)), 
					xlim = c(spatial.extent.list$lon.min, 
							spatial.extent.list$lon.max), 
					ylim = c(spatial.extent.list$lat.min, 
							spatial.extent.list$lat.max), 					
					#world.map=T,
					#world.map.lwd=1.0,,
					county.map=T,
					county.map.lwd=0.25,
					county.map.col="grey",
					state.map=T,
					state.map.col="grey",
					axes=T)
			# Add CLO Copyright 
		mtext(side=1, # (1=bottom, 2=left, 3=top, 4=right)
				line=2,
				text = "Cornell Lab of Ornithology (c) 2011",
				cex = 0.5)

		day_of_year<-srd.stm.ave$times[iii]
		day_of_year<-round( 365*(day_of_year-2011))
		date_string<-format(strptime(day_of_year, format="%j"), format="%B %d, 2011")
		title(date_string)
		dev.off()
	} # iii
	mcl_list<-as.list(1:length(srd.stm.ave$times))
	mclapply(mcl_list,mc_plot_wrapper)

	event.logger(event.tag = "3km maps completed", con = log.con)
# ------------------------------------------------------------------------
# } # END IF 3KM 	
# ------------------------------------------------------------------------



# ----------------------------------------------------------------------------- 
# Performance Assessment
# For Replicates within Samples Design
# -----------------------------------------------------------------------------
# 	* Accumulate test set prediction fold average
# 	* Global performance & ROC plot
# 	* Monthly performance & performance plots
# ----------------------------------------------------------------------------- 
	# -------------------------------------------------------------------------
	# Accumulate Test Set Predictions Across Folds
	# -------------------------------------------------------------------------
	load(file=paste(exp.rdata.dir, exp.tag,".erd.test.data.RData",sep=""))
	pp.dir <- paste(stem.dir, "/pp/",sep="")
	pp.ave.dir <- paste(stem.dir, "/pp.ave/",sep="")
	dir.create(pp.ave.dir, showWarnings = F)
	# --------------------------------------------
	pred.fold.ave <- data.frame(
		yyy = erd.test$locs$y, 
		xxx = erd.test$locs$x,
		date = erd.test$dates,
		mean = rep(0, length(erd.test$locs$y)),
		var = rep(0, length(erd.test$locs$y)), 
		count =  rep(0, length(erd.test$locs$y)),
		obs = erd.test$y)
	rm(erd.test)
	# Average over data samples	
	for (jjj in c(1:n.samples)){
	 	f.begin <- (jjj-1)*n.replicates+1
	 	f.end <- jjj*n.replicates
	 	#cat(jjj, f.begin, f.end, "\n")
		ttt.mean <- rep(0, nrow(pred.fold.ave))
		ttt.count <- rep(0, nrow(pred.fold.ave))
		# Average over partition replicates within samples 
		for (fff in c(f.begin:f.end)){
			load(file=paste(pp.dir,"pred.stem.",fold.list[fff],".RData",sep=""))
			fff.index <- !is.na(pred.stem$mean)
			ttt.count[fff.index] <- ttt.count[fff.index] + 1
			ttt.mean[fff.index] <- ttt.mean[fff.index] + pred.stem$mean[fff.index]
			rm(fff.index, pred.stem)          
		}
		na.ind <- (ttt.count > 0)
		ttt.mean[na.ind] <- ttt.mean[na.ind]/ttt.count[na.ind]
		# ---
		ttt.index <- !is.na(ttt.mean)	        
		pred.fold.ave$count[ttt.index] <- pred.fold.ave$count[ttt.index] + 1 
		pred.fold.ave$mean[ttt.index] <- pred.fold.ave$mean[ttt.index] + 
					ttt.mean[ttt.index]  
		pred.fold.ave$var[ttt.index] <- pred.fold.ave$var[ttt.index] + 
					(ttt.mean[ttt.index]) ^ 2 
		rm(ttt.mean, ttt.count, ttt.index, f.begin, f.end)
	} #jjj 
	na.ind <- (pred.fold.ave$count > 0)
	pred.fold.ave$mean[na.ind] <- pred.fold.ave$mean[na.ind]/
						pred.fold.ave$count[na.ind]
	na.ind2 <- (pred.fold.ave$count >= 2)
	pred.fold.ave$var[na.ind2] <- pred.fold.ave$var[na.ind2]/ 
			(pred.fold.ave$count[na.ind2] -1) -
			(pred.fold.ave$mean[na.ind2]^2)*(pred.fold.ave$count[na.ind2])/
			(pred.fold.ave$count[na.ind2] -1)
	# required by eq.wt.pp.R
	pred.fold.ave$pred <- pred.fold.ave$mean 
	# Save 
	save(pred.fold.ave, file= paste(pp.ave.dir,"pred.fold.ave.RData", sep=""))

# -------------------------------------------------------------------------
# Check for test sets identically equal to 0 or 1, 
# This is especially a concern for very rare species 
if ( sum(as.numeric(pred.fold.ave$obs)-1) != 0 &
		sum(as.numeric(pred.fold.ave$obs)-1) != 
			length(pred.fold.ave$obs) ){
	# -------------------------------------------------------------------------
	# Global Performance Assessment:
	# Annual Predictive Performance without spatial subsampling 
	# This approach is computationally cheap and is useful for 
	# selecting thresholds for predicting response classes from 
	# predicted probabilities. 
	# -------------------------------------------------------------------------
	# Obs must be a numeric for presence.absence.accuracy()	
	pa.data <- data.frame(tag="global", 
	                        obs = as.numeric(pred.fold.ave$obs)-1, 
	                        ppp = pred.fold.ave$mean)
	pp.threshold <- seq(from=0.01, to=0.99, length=100)
	pa.metrics <-presence.absence.accuracy(pa.data, 
						threshold=pp.threshold,
						na.rm=T)
	write.csv(pa.metrics, file=
			paste(pp.ave.dir,"global.pa.metrics.csv",sep=""))
	# --------------------
	map.file.name <- paste(pp.ave.dir,"global.auc.roc.plot.png",sep="")	
	if (plot.type == "PNG") {
		png(file = map.file.name, 
				width = 1500, 
				height= 1500,
				units = "px")
				#res = 300) 
	}
	if (plot.type == "bitmap") {
		bitmap(file = map.file.name, 
				width = 1500, 
				height= 1500,
				units = "px",
				res = 300) 
	}			
	par(oma=c(2,2,2,2), cex=1.0)
	auc.roc.plot(	pa.data,
			opt.thresholds=TRUE,
 			opt.methods=c("MaxKappa","PredPrev=Obs","MinROCdist"))
	dev.off()
	event.logger(event.tag = "global pp", con = log.con)	
	# -----------------------------------------------------------------	
	# Compute Monthly Predictive Performance 
	# with Uniform Eq. Wt. Subsampling 
	#	
	###### 
	# Note: for small regions 
	# It may not be possible to estimate 
	# monthly intervals. 
	# setting pp.n.time.intervals <- 1 works. 
	# Also note, that the grid parameters need to be small enough
	#  	pp.grid.cell.min.lat=0.15, 
	# 	grid.cell.min.lon=0.15, 	
	#	
	########
	# TODO: 
	# Push parameters up to control file
	# 	pp.n.time.intervals <- 12
	#	pp.n.mc <- 25
	#	pp.min.testset.sample.size <- 25
	#	pp.grid.cell.min.lat=0.15, 
	#	pp.grid.cell.min.lon=0.15, 
	#	pp.max.cell.locs=2, 	
	# -----------------------------------------------------------------	
	#pp.n.time.intervals <- 1	
	ttt.year <- floor(begin.date)
	ttt <- seq(from=1, to=365,  length= pp.n.time.intervals+1 ) 
	begin.mdate <- ttt.year + round(ttt[1:pp.n.time.intervals])/365
	end.mdate <- ttt.year + round(ttt[2:(pp.n.time.intervals+1)])/365
	#cbind(begin.mdate,end.mdate)
	# ---------------------------
	#pp.n.mc <- 25 # The number of mc trials
	#pp.min.testset.sample.size <- 25 # per monthly interval
	monthly.map.pp <- matrix(0,  pp.n.mc*pp.n.time.intervals, 13)
	pp.threshold <- seq(from=0.01, to=0.99, length=100)
	for (iii in 1: pp.n.time.intervals){
		ttt.index <- pred.fold.ave$date > begin.mdate[iii] & 
					pred.fold.ave$date <= end.mdate[iii]	
		if (sum(ttt.index) >= pp.min.testset.sample.size) {
			# Select Threshold month by month
			# Obs must be a numeric for presence.absence.accuracy()	
			pa.data <- data.frame(tag="global", 
						obs = as.numeric(pred.fold.ave$obs[ttt.index])-1, 
						ppp = pred.fold.ave$mean[ttt.index])
			if ( var(pa.data$obs)  > 0 ){ 
				# Select Threshold to compute pp stats
				pa.metrics <-presence.absence.accuracy(pa.data, 
									threshold=pp.threshold,
									na.rm=T)
				# Obs must be a FACTOR with levels "TRUE" and "FALSE"
				# for eq.wt.occurrence.map.pp()				
				stem.map.pp <- eq.wt.occurrence.map.pp(
						st.pred.obj = pred.fold.ave[ttt.index, ] , 
						# eq. wt. grid inits 
						n.mc= pp.n.mc  , # number of MC iterations 
						grid.cell.min.lat= pp.grid.cell.min.lat, 
						grid.cell.min.lon= pp.grid.cell.min.lon, 
						max.cell.locs= pp.max.cell.locs, 
						# predictive performance stat inits 
						threshold= pa.metrics$threshold[which.max(
											pa.metrics$Kappa)],  
						min.testset.sample.size=pp.min.testset.sample.size)
				# ---------------
				# Store Results - stack the stem.map.pp.$eq.wt.pp data.frames
				# and add information on temporal slices
				# ---------------
				ttt.index <- c( ((iii - 1)*pp.n.mc + 1) : (iii*pp.n.mc) )
				#cat(iii, dim(stem.map.pp$eq.wt.pp), "\n")
				monthly.map.pp[ ttt.index, c(1:9)] <- as.matrix(
							stem.map.pp$eq.wt.pp[ c(1:pp.n.mc), c(1:9)])
				monthly.map.pp[ ttt.index, 13] <- pa.metrics$threshold[
							which.max(pa.metrics$Kappa)]
				ttt.stem.map.pp <- names(stem.map.pp$eq.wt.pp)
				rm(stem.map.pp)
		} # var(pa.data$obs)  > 0 )
		rm(pa.data)
		} # pp.min.testset.sample.size
		ttt.index <- c( ((iii - 1)*pp.n.mc + 1) : (iii*pp.n.mc) )
		monthly.map.pp[ ttt.index, 10] <- begin.mdate[iii]
		monthly.map.pp[ ttt.index, 11] <- end.mdate[iii]
		monthly.map.pp[ ttt.index, 12] <- iii 
		# ------------
		rm(ttt.index)
	}# end iii
	monthly.map.pp <- as.data.frame(monthly.map.pp)
	names(monthly.map.pp) <- c(ttt.stem.map.pp, 
				"begin.mdate", "end.mdate", "interval", "threshold")
	write.csv(monthly.map.pp, file=paste(pp.ave.dir,
				"monthly.design.map.pp.csv",sep=""))
	# ------------------
	# Monthly PP Plots
	# ------------------
	# unknown as to why this does not work. 
	#monthly.map.pp[is.nan(monthly.map.pp)] <- 0
	monthly.map.pp[is.na(monthly.map.pp)] <- 0
	pp.metric <- c("rmse", "pcc", "sensitivity", 
					"specificity", "kappa", "auc")
	for (iii.pp in c(1:6)){
		map.file.name <- paste(pp.ave.dir,"monthly.",pp.metric[iii.pp],
							".plot.png",sep="")
		if (plot.type == "PNG") {
			png(file = map.file.name, 
					width = 1500, 
					height= 1500,
					units = "px")
					#res = 300) 
		}
		if (plot.type == "bitmap") {
			bitmap(file = map.file.name, 
					width = 1500, 
					height= 1500,
					units = "px",
					res = 300) 
		}
		par(oma=c(2,2,2,2), cex=1.0)
		boxwex <- 0.5
		box.formula <- as.formula(paste(pp.metric[iii.pp], " ~ interval"))
		boxplot(formula = box.formula, 
					data = monthly.map.pp,
			boxwex = boxwex,
			col = "orange",
			ylab = "Error Rate",
			main = paste(pp.metric[iii.pp]), 
			axes = F)
		axis(2)
		axis(1, at = c(1:12), 
			labels = c("J","F","M","A","M","J","J","A","S","O","N","D"))
		box()
		dev.off()	
	}
	event.logger(event.tag = "monthly pp", con = log.con)
} # Check for test sets identically equal to 0 or 1, 
# -----------------------------------------------------------------------------

	
# -----------------------------------------------------------------------------
# -----------------------------------------------------------------------------
# Combine Predictor Importance (PI) across folds
#
# Each column of pi.data contains stixel centroids and predictors
# For each row the location and importances for a single base model are 
# recorded. These rows are combined across folds to facilitate
# study of PI patterns in space and time, taking into account
# sampling (fold) variation. As an example, these values are 
# processed for use in Bird Vis later in the workflow.
#
# TODO: Replicates within Samples Design
# -----------------------------------------------------------------------------
# Need to take into account the replicated sample design 
# when processing PI data for workflow products downstream.  
# -----------------------------------------------------------------------------
# -----------------------------------------------------------------------------
	pi.dir <- paste(stem.dir,"pi/",sep="")
	dir.create(path= pi.dir , showWarnings = FALSE)
	#pi.data <- pi.data.combine(pi.dir, fold.list) 
	locations <- NULL
	pi <- NULL
	for (fff in c(1:length(fold.list))){ 
		load(file=paste(pi.dir,"pi.",fold.list[fff],".RData", sep=""))
		pi <- rbind(pi, pi.data$pi) 
		locations <- rbind(locations, pi.data$location)
		rm(pi.data)
	} 
	# -----------------------------------
	pi.filename <- paste(pi.dir, "combined.pi.data.csv",sep="")
	write.csv(pi, file=pi.filename)
	pi.filename <- paste(pi.dir, "combined.pi.locations.csv",sep="")
	write.csv(locations, file=pi.filename)
	event.logger(event.tag = "accumulate pi", con = log.con)

# -----------------------------------------------------------------------------
# Clean up 
# -----------------------------------------------------------------------------
event.logger(event.tag = "end wf.fold.summary", con = log.con)
close(log.con)
# -----------------------------------------------------------------------------
