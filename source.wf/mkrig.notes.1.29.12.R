



nnn <- 200
halloween.palette <- colorRampPalette(
	c("black","grey20","#FF7100","white"),
	bias=2.0)
col <- halloween.palette(100)
z.max <- 0.75
require(fields)
require(maps)
#quilt.plot(x=srd.stm.ave$locs$lon[ttt.index], 
#			y=srd.stm.ave$locs$lat[ttt.index], 
#			z= srd.stm.ave$mean[ttt.index,time.index], 
#			col=col,
#			nrow=nnn ,
#			ncol=nnn)
#map("state", add=T, col="grey")
#map("county", add=T, col="grey", lwd=0.25)
# -------------------
# Average SRD - averaged across design
# -------------
load("/Users/Fink/projects/2011_TG_Atlas/Calibration_Test/NYS_1_Ovenbird/stm.30k.ave/srd.30km.stm.RData")

ttt.index <- sample( x = c( 1:length(srd.stm.ave$locs$lon)), 
					size = 2000)					
# ------------------
for (ttt in 1:10){
	par(mfrow=c(1,1), bg="black", mar=c(2,2,2,2), cex=0.5)
	time.index <- ttt	
	png(file=paste("/Users/Fink/projects/2011_TG_Atlas/Calibration_Test/",
					"NYS_1_Ovenbird/stm.30k.ave/srd.30km.stm.",ttt,
					".png",sep=""),
			width=1000, 
			height=800,
			bg="black")
	ddd <- srd.stm.ave$mean[ttt.index,time.index]
	ddd[ ddd > z.max ] <- z.max
	ddd[ ddd < 0 ] <- 0
	d.mk <- mKrig(x = srd.stm.ave$locs[ttt.index, c(2,1) ] , 
				y = ddd,
				theta=2.0,  # matern parameter ==> exponential 			
				lambda= 0.25 ) # smoothing parameter
	surface.mk <- predict.surface(d.mk,				nx=nnn,
				ny=nnn)
	surface(surface.mk,
				#nrow=nnn, 
				#ncol=nnn,
				col=col,
				type="I",
				zlim=c(0,z.max))		
	map("state", add=T, col="grey")
	map("county", add=T, col="grey", lwd=0.25)
	rm(ddd)
	dev.off()
}

# -------------




# ----------------------------------------------------------------
#  
#
#
# Try out GCV on a grid of lambda's.
# For this small data set 
# one should really just use Krig or Tps but this is an example of
# approximate GCV that will work for much larger data sets using sparse 
# covariances and the Monte Carlo trace estimate
#
# a grid of lambdas:
lgrid<- 10**seq(-1,1,,15) 

GCV<- matrix( NA, 15,20)
trA<-  matrix( NA, 15,20)
GCV.est<- rep( NA, 15)
eff.df<- rep( NA, 15)
logPL<- rep( NA, 15) 
# loop over lambda's
for (  k in 1:15){
out<- mKrig( x = srd.stm.ave$locs[ttt.index, c(2,1) ] , 
			y = srd.stm.ave$mean[ttt.index,time.index],
       theta = 2.0, 
       lambda=lgrid[k])
     	#cov.function="stationary.taper.cov",
     	#Taper="Wendland",  Taper.args=list(theta = 1.5, k=2, dimension=2)  ) 
GCV[k,]<- out$GCV.info
trA[k,]<- out$trA.info
eff.df[k]<- out$eff.df
GCV.est[k]<- out$GCV
logPL[k]<- out$lnProfileLike
}
#
# plot the  results different curves are for individual estimates  
# the two lines are whether one averages first the traces or the GCV criterion.
#
par( mar=c(5,4,4,6), cex=0.5)
matplot( trA, GCV, 
	type="l", 
	col=1, 
	lty=2, 
	xlab="effective degrees of freedom", 
	ylab="GCV")
lines( eff.df, GCV.est, lwd=2, col=2)
lines( eff.df, rowMeans(GCV), lwd=2)



			
			 