
# -------------------------------------------------------------------------------
# -------------------------------------------------------------------------------
# eBird HPC Make Resampling Indices
# Nick Bruns extending Daniel Fink 
# CLO 
# 8.30.2013
# 
# Based on training data locations build resampling indices for 
# STEM ensemble. Use design in train.test.design(). 
#
# Description: Sampling
# ------------------------
# For each data sample is created by sampling without replacement
# from the unique training data locations. 
# We use unique locations to break two sources of correlation 
# between training and test sets. By using unqiue locations
# we remove some of the spatial correlation between train and test set. 
# Since all observations from each unqiue location go into only
# one of train and test sets, we also remove correlation 
# induced for eBirders' that carry out repeated sample at the 
# same location at different times. 
# 
# Depending on the specific use of the distribution estimate, 
# there may be better ways to resample the data.
# This is a starting point. 
#
# 2.7.12
# ----------
# It turns out that the unique location splitting is computationally
# expensive for very large numbers of locations. 
# So, I am going to fall back to a straight bootstrap design, 
# sampling with replacement. 
#
# 
#
# Description: Design
# ---------------------
# Each sample is repeated a specified number of times so that
# ensemble-specific effects can be averaged out. 
# This facilitates a simple analysis of variance where
#
# 	P_{s,t}(sample_i, replicate_j) = 
#		/beta_0 + sample_i + replicate_j + epsilon
# 	where
#		P_{s,t}(sample_i, replicate_j) is the prediction 
# 			at location s and time t from sample i and 
# 			replicate j nested within i. 
#			The total sample size is I*J where 
# 			i=1, .... I and j = 1,..., 10. 
# 		sample_i ~ N(0, /sigma^2_sample)
# 		replicate_j ~ N(0, /sigma^2_replicate) 
# 	and replicates are nested within samples. 
# 
# The ANOVA is used to average out variation associated with 
# model fiting/estimation from sampling variation. 
# This is important because its the sampling variation that 
# we use to make inference. I.e. better estimates of this 
# sampling variation means better inference including 
# assessing uncertainty of estimates.  
#
# 	n.samples - this is the number of samples of unique locations 
# 			to include in the design
# 	n.replicates - this is the number of replicates of each design to 
# 			include. E.g. with STEM replicates are used to 
# 			average out effects of partitioning.  
#
# TODO:
# ------------------------
#  
#
# MISC:
# ------------------------
# 		cd /mnt/data3/TG_2011/Calibration_Test/source.atlas.10.03/ 
#		nohup R --vanilla < 
#			wf.make.training.samples.12.05.R > 
#			wf.make.training.samples.12.05.out.txt &
#
#		setwd("/Users/Fink/projects/2011 TG Atlas/Calibration_Test/source.resampling/")
#		source("wf.data.prep2_erd.training.samples.R")
# -------------------------------------------------------------------------------
# -------------------------------------------------------------------------------


	# --------------------------------------------
	# Source & Packages
	# --------------------------------------------
	source(paste(source.dir,"vMR.library.ttt.01.07.12.R",sep=""))
	source(paste(source.dir,"vMR_baseModels.12.05.R",sep=""))
	source(paste(source.dir,"event.logger.R",sep=""))	
	# --------------------------------------------
	# LONESTAR:
	# job.id & fold.id are passed on the command line	
	# --------------------------------------------
	#options <- commandArgs()
	## print(options)
	## BUG Carryover from TACC_TSK_ID which starts at 0
	#fff <- as.numeric(options[4])
	#fold.list <- fff
	#job.id <- as.numeric(options[5]) 
	##print(paste("Fold ID = ", fff, sep=""))
	##print(paste("job.id = ", job.id, sep=""))
	#source(paste(source.dir,"wf.job.control.R",sep=""))	

	# --------------------------------------------
	# Event Logging	
	# --------------------------------------------
	log.file <- paste(stem.dir, "wf.training.samples.log.txt", sep="")
	log.con <- event.logger(file.name = log.file, initiate = T)
	event.logger(event.tag = paste("begin.training.samples"), con = log.con)
	
	# --------------------------------------------
	# Loop over Jobs	
	# --------------------------------------------
	for (job.id in 1:length(job.list$COMMON_NAME)) { 	
		#job.id <- 2
# commenting out because sensitive business
	#	source(paste(source.dir,"wf.control_experiment.R",sep=""))	
		# --------------------------------------------
		# Load Training Data Locations 	
		# --------------------------------------------
		load(file=paste(exp.rdata.dir,
				exp.tag,".erd.train.locations.RData",sep=""))
		#str(locations)
		# --------------------------------------------
		# Make Sample Indices	
		# -------------------------------------------
		# n.samples & n.replicates needs to come from the Job Control File 
		# -----------
		sample.list <- train.test.split.design(
			locations = data.frame(
							x=locations$lon ,
							y=locations$lat), 
			n.samples = n.samples,
			n.replicates = n.replicates,
			percent.train = 0.63, 
			m.frac = 1)
		# --------------------------------------------
		# Write Indices	to Disk
		# --------------------------------------------
		save(sample.list,
			file=paste(exp.rdata.dir, exp.tag,
				".training.samples.RData",sep=""))		
	}
	# --------------------------------------------
	# End Logging	
	# --------------------------------------------
	event.logger(event.tag = paste("end.training.samples"), con = log.con)
	close(log.con)







