# -----------------------------------------------------------------------------
# -----------------------------------------------------------------------------
# stem.gbm.partial.dependence
# 7.14.11
# 8.26.11
# 9.26.11 - Modify stem.gbm.predictor.importance.R
#
# This function creates a data frame containing the predictor 
# importance (PI)s from an ensemble of base models. 
# PI's can be summarized for one or many of the partitions
# in the ensemble. Location and partition number of each base 
# model is stored in a separate location data.frame.  
#
# It is important to account for the situation where predictors 
# may be missing from a given base.model. To do this there is a step 
# that checks for missing predictors and packs the results matrix against 
# a master set of predictors.
#
# INPUT:
# -------------
# 	stem.object - the model object
# 	ensemble.members - the list of partitions to summarize
#	n.trees - the gbm parameter
#	predictor.names - vector of character strings that is 
# 		the master predictor set. 
#
# OUTPUT:
# ------------------ 
# List with two dataframes:
#
# locations - contains the center (average) of each partition cell
#			dimension, plus one additional column for the 
# 			partition number. 
# pi - these are the predictor importances, one for each column. 
# 	Each row contains a single base model supported by a single stixel. 
# 	The row order is the same as the parition order. Note that there will 
# 	be NA's stored in the locations and pi's for partition cells without
# 	an associated base model. This will happen when the minimum 
# 	base model sample size is not met. 
#
# Note also, that for GBM it is possible to have a "degenerate" 
# model object which does not have any predictors. These models
# are defined as gbm.object$fit is identically -Inf
# and gbm.object$initF is -Inf. These model objects 
# cause errors for summary.gbm() so we do not use them and pack 
# NA's in the result data.frames. 
#
# -----------------------------------------------------------------------------
# -----------------------------------------------------------------------------
stem.gbm.partial.dependence <- function(
	stem.object,
	effect.names,
 	ensemble.members = NULL, 
	n.trees) {
# -------------------------------
# ------------------------------- 
if (is.null(ensemble.members))
	ensemble.members <- c(1:length(stem.object$model))
# Calculate total number of basemodels
nmodels <- 0
for (iii in 1:length(ensemble.members)){
	nmodels <- nmodels + length(stem.object$model[[ ensemble.members[iii] ]])
}
# Inits
pd.data <- vector("list", nmodels)
pi.locs <- matrix(NA,
	nmodels, 
	ncol(stem.object$partition[[1]]$min) + 1)
pi.locs <- as.data.frame(pi.locs)
names(pi.locs) <- c(names(stem.object$partition[[1]]$min),"partition")
# Loop over ensemble members (partitions)
iii <- 0 # counter
for (pn in ensemble.members){ 
	# Loop over all base-models
	for (mn in 1:length(stem.object$model[[pn]]) ){
		iii <- iii + 1		
		partition.cell.number <- as.numeric(names(stem.object$model[[pn]])[mn]) 
		base.model.obj <- stem.object$model[[pn]][mn][[1]]
		model.na <- (length(base.model.obj)==1 & 
						is.na(base.model.obj[[1]][1]) )		
		# Note that for gbm models it is possible to fit a model 
		# for which the gbm.object$fit is identically -Inf
		# and gbm.object$initF is -Inf. These model objects 
		# cause errors for summary.gbm()
		# So we check for these too. 
		if (!model.na) model.na <- is.infinite(base.model.obj$initF)
		#if (model.na) 
		#	cat(" base.model from cell ", partition.cell.number, " is NA \n") 
		if (!model.na){
		# Check to see if predictors specified by 
		# pd.effects were actually used 		
		if (sum(effect.names %in% base.model.obj$var.names)	
			== length(effect.names)){	
			# --------------------------------- 			
			# Compute Partition Cell Centroid( mean )	
			pi.locs[iii,] <- 
				c(	
				(stem.object$partition[[pn]]$min[partition.cell.number,] +
				 stem.object$partition[[pn]]$max[partition.cell.number,] )/2,
				 pn) # add partition number
			# Compute Predictor Importance
			# --------------------------------- 
			# NOTE - Its assumed that the object 
			# returned from summary.gbm has certain structural elements
			# its a data.frame with rows corresponding to different predictors. 
			# The predictor names are in the first column and 
			# the second column, named "rel.inf" with gbm, contains the 
			# relative importances corresponding to each of the predictors. 
			# The order of the rows does not matter because
			# of the predictor name matching that goes on below.  
				pd.data[[iii]] <- plot.gbm(
						base.model.obj,
						effect.names,
						n.trees, 
						return.grid=T)			
			
		} # end if predictors
		} # end if (!model.na){
		# -------------------------------
		rm( model.na, base.model.obj, partition.cell.number)		
	} # end mn	
	} # end pn
# ---------------------------------------------	
return(list(location = pi.locs,
			pd = pd.data))
} #end function
# -----------------------------------------------------------------------------
# -----------------------------------------------------------------------------
